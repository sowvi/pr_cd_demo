CC=gcc
OBJ= check_evenness.o find_largestnumber.o

OBJDIR=bin

# Compile an executable named yourProgram from yourProgram.c
all: check_evenness.c find_largestnumber.c 
	gcc -g -Wall -o ./$(OBJDIR)/check_evenness check_evenness.c
	gcc -g -Wall -o ./$(OBJDIR)/find_largestnumber find_largestnumber.c

clean:
	rm -rf $(OBJDIR)/*


